/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.NumeroMayor;

/**
 *
 * @author madar
 */
public class TestNumeroMayor {

    public static void main(String[] args) {
        NumeroMayor n = new NumeroMayor(34521);
        System.out.println(n.toString());

        //No importa si T o F
        NumeroMayor n2 = new NumeroMayor(3452991, false);
        System.out.println(n2.toString());

//        n2.ordenaBurbuja();
//        System.out.println("Número mayor:\n "+n2.toString());

        System.out.println("Numero mayor:"+n2.getNumeroMayor());

    }

}
